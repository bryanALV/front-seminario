class Constants {
  static const String urlAuthority = "";

  static const String contenTypeHeader = "application/json";
  static const String pathServiceProduct = "";
  static const String pathServiceProductUpdate = "";
  static const String pathServiceStatusUpdate = "";
  static const String pathServiceDiscardDelete = "";
  static const String createMessage = "Creación exitosa";
  //product
  static const String titleProduct = "Registro de producto";
  //Router
  //static const String homeRoute = "/";
  static const String registroRoute = "/productpage";
  static const String productRoute = "product/Productpage";
  //Mensajes
  static const String labelRegistro ="Registro";
  static const String labelSerial = "Serial";
  static const String labelSerialII = "Serial del producto";
  static const String labelDiscard = "Despachar Producto";
  static const String labelEstado = "Estado de producto";
  static const String labelEstadoII = "Estado";
  static const String labelNombre = "Nombre del producto";
  static const String labelNombreII = "Nombre";
  static const String labelCantidad = "Cantidad del Producto";
  static const String labelCantidadII = "Cantidad";
  static const String labelFecha = "Fecha del producto";
  static const String labelFechaII = "Fecha";
}
