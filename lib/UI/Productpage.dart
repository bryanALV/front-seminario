import 'package:flutter/material.dart';
import 'package:proyectov2/resource/constants.dart';
class Productpage extends StatefulWidget {
  Productpage({Key key}) : super(key: key);

  @override
  _ProductpageState createState() => _ProductpageState();
}

class _ProductpageState extends State<Productpage> {
    String _nombre;
  String _serial;
  String _numero;
  String _fecha;
   TextEditingController _inputDate = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
    appBar: AppBar(
          title: Text('Registro De los Productos'),
        ),
        body: ListView(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          children: <Widget>[
            TextField(
                  textCapitalization: TextCapitalization.sentences,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
                      counter: Text('Letras 0'),
                      hintText: Constants.labelNombre,
                      labelText: Constants.labelNombreII,
                      suffix: Icon(Icons.create),
                      icon: Icon(Icons.account_box)),
                  onChanged: (valor) {
                    _nombre = valor;
                    print(_nombre);
                  },
                ),
                //serial
             TextField(
                  textCapitalization: TextCapitalization.sentences,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
                      counter: Text('Letras 0'),
                      hintText: 'Serial Del Producto',
                      labelText: 'Serial',
                      suffix: Icon(Icons.format_list_numbered),
                      icon: Icon(Icons.format_align_right)),
                  onChanged: (valor) {
                    _serial = valor;
                  },
                ),
                //cantidad
                TextField(
                  //textCapitalization: TextCapitalization.sentences,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
                      counter: Text('Letras 0'),
                      hintText: Constants.labelCantidad,
                      labelText: Constants.labelCantidadII,
                      suffix: Icon(Icons.content_paste),
                      icon: Icon(Icons.control_point)),
            
                  onChanged: (valor) {
                    _numero = valor;
                  },
                ),
                //fecha
                TextField(
                  enableInteractiveSelection: false,
                  controller: _inputDate,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
                      hintText: Constants.labelFecha,
                      labelText: Constants.labelFechaII,
                      suffix: Icon(Icons.calendar_today),
                      icon: Icon(Icons.calendar_today)),
                  onTap: () {
                    FocusScope.of(context).requestFocus(new FocusNode());
                    _selectDate(context);
                  },
                ),
                RaisedButton(
  color: Colors.green,
  child: Text('Registrar Producto'),
  onPressed: (){}),], 
                    ),
                    );
              }
            
            //  Widget _crearInput() {
              //  return 
              //}
            
              //
              //
            
              //Widget _crearSerial() {
                //return
                //
                //
                
            //  }
            
            //  Widget _crearCantidad() {
              ///  return 
             // }
            
              ///
             // Widget _crearFecha(BuildContext context) {
               // return 
              //}
            _selectDate(BuildContext context) async {
                DateTime picked = await showDatePicker(
                  context: context,
                  initialDate: new DateTime.now(),
                  firstDate: new DateTime(2020),
                  lastDate: new DateTime(2099),
                );
              
                
                if (picked != null) {
                  setState(() {
                    //redibujar
                    _fecha = picked.toString();
                    _inputDate.text = _fecha;
                  });
                }
                
              }
            
        //      Widget _crearButton(BuildContext context) {
          //  return Column(
            //  children: <Widget>[

                
             // }
 



}
