import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:proyectov2/UI/Productpage.dart';
import 'package:proyectov2/UI/Statuspage.dart';
import 'package:proyectov2/UI/product/Discard.dart';
//import 'package:proyectov2/UI/product/Productpage.dart';
import '../navigator.dart';

class Homepage extends StatelessWidget {
    
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar( title: Text('Mi aplicacion'),
      backgroundColor: Colors.deepOrange,
      
     ),
body: ListView(children: <Widget>[
Center(child: Text('animacion2')),
Container(
  height: 500,
  child: FlareActor(
    "assets/Filip.flr", 
    animation: "phone_sway",
    fit: BoxFit.fitWidth
    ),
)
],),


     drawer: Drawer(
       child: ListView(
         children: <Widget>[
           DrawerHeader(
             decoration: BoxDecoration(
               image: DecorationImage(image: NetworkImage("https://escuela-emprendedores.alegra.com/wp-content/uploads/2018/08/inventario.jpg"))
             ),
             child: Text('a')),
           Custum(),
           Divider(),
           Delete(),
           Divider(),
           Modific()
       ],)
     ),
    );
  } 
} 

class Custum extends StatelessWidget {
  const Custum({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InkWell(
        splashColor: Colors.green,
       // onTap: () {
         //     TecniNavigator.goToRegistroProduct(context);
           // },
        onTap: ()=>{
             Navigator.of(context).push(
                MaterialPageRoute(builder: (BuildContext context)=>Productpage())
               ),
        },
        child: Container(
          height: 40,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(children: <Widget>[
 Icon(Icons.add_shopping_cart),
 Padding(
   padding: const EdgeInsets.all(8.0),
   child: Text('Registrar Producto',style: TextStyle(fontSize: 25.0)),),
              
              ],
              ),
             Icon(Icons.arrow_right)
            ],
          ),
        ),
        
      ),
    );
  }
}

class Delete extends StatelessWidget {
  const Delete({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InkWell(
        splashColor: Colors.green,
        onTap: ()=>{
                       Navigator.of(context).push(
                MaterialPageRoute(builder: (BuildContext context)=>Discarpage())
               ),
        },
          //   Navigator.of(context).push(
            //    MaterialPageRoute(builder: (BuildContext context)=>Productpage())
              // ),
        //},
        child: Container(
          height: 40,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(children: <Widget>[
 Icon(Icons.delete),
 Padding(
   padding: const EdgeInsets.all(8.0),
   child: Text('Eliminar Producto',style: TextStyle(fontSize: 25.0)),),
              
              ],
              ),
             Icon(Icons.arrow_right)
            ],
          ),
        ),
        
      ),
    );
  }
}

class Modific extends StatelessWidget {
  const Modific({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
   return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InkWell(
        splashColor: Colors.green,
        onTap: ()=>{
             Navigator.of(context).push(
                MaterialPageRoute(builder: (BuildContext context)=>Statuspage())
               ),
        },
        child: Container(
          height: 40,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(children: <Widget>[
 Icon(Icons.create),
 Padding(
   padding: const EdgeInsets.all(8.0),
   child: Text('Modificar Producto',style: TextStyle(fontSize: 25.0)),),
              
              ],
              ),
             Icon(Icons.arrow_right)
            ],
          ),
        ),
        
      ),
    );
  }
}