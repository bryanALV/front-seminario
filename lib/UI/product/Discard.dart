import 'package:flutter/material.dart';
import 'package:proyectov2/resource/constants.dart';

class Discarpage extends StatelessWidget {
  const Discarpage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Despacho del producto'),
      ),
      body: ListView(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          children: <Widget>[
            TextField(
              textCapitalization: TextCapitalization.sentences,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20.0)),
                  counter: Text('Letras 0'),
                  hintText: Constants.labelSerialII,
                  labelText: Constants.labelSerial,
                  suffix: Icon(Icons.create),
                  icon: Icon(Icons.account_box)),
              onChanged: (valor) {
                var _nombre = valor;
                print(_nombre);
              },
            ),
            //
            
                RaisedButton(
                  child: Text('Despachar producto'),
                  color: Colors.green,
                  onPressed: (){}),
            //
          ]),
    );
  }
}
