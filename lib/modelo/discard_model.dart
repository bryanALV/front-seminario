import 'package:proyectov2/modelo/product_model.dart';

class Discard{
  Product product;
  String reason;

  Discard({this.product, this.reason});

  factory Discard.fromJson(Map<String, dynamic> parsedJson){
    return Discard(
      reason: parsedJson['reason'],
      product: Product.fromJson(parsedJson['product'])
       );
  }

  Map<String, dynamic> toJson()=>{
    'reason': reason,
    'product':product.toJson(),
  };

}