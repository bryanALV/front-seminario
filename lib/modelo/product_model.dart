
class Product{
  int id;
  String nombre;
  String cantidad;
  String pais;
  String fecha;
Product({
  this.id,
  this.nombre,
  this.cantidad,
  this.pais,
  this.fecha
});
factory Product.fromJson(Map<String, dynamic> parsedJson){
  return Product(
  id: parsedJson['id'],
  nombre: parsedJson['nombre'],
  cantidad: parsedJson['cantidad'],
  pais: parsedJson['pais'],
  fecha: parsedJson['fecha'],
  );
}
  Map<String, dynamic> toJson()=>{
  'id':id,
  'nombre':nombre,
  'cantidad':cantidad,
  'pais':pais,
  'fecha':fecha,
  };

}