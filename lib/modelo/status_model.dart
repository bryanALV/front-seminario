
import 'package:proyectov2/modelo/product_model.dart';

class Status{
  Product product;
  String desc;
  
  Status({
    this.product,
    this.desc
  });
  factory Status.fromJson(Map<String, dynamic> parsedJson){
    return Status(
      product: Product.fromJson(parsedJson['product'])
    
    );
  } 
  Map<String, dynamic>toJson()=>{
    'desc': desc,
    'product': product.toJson(),
  };
}