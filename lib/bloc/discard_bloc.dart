import 'package:flutter/material.dart';
import 'package:proyectov2/modelo/api_response_model.dart';
import 'package:proyectov2/modelo/discard_model.dart';
import 'package:proyectov2/repository/repository.dart';
import 'package:proyectov2/resource/constants.dart';

class DiscardBloc{
  final Repository _repository = Repository();
    var _apiResponse = ApiResponse();

  ApiResponse get apiResponse => _apiResponse;

  DiscardBloc(BuildContext context);
  Future<ApiResponse> deleteProduct(Discard discard) async {
    ApiResponse apiResponse = await _repository.eliminarproduct(discard);
    if (apiResponse.statusResponse == 200) {
      apiResponse.message = Constants.createMessage;
      print(apiResponse.message);
    } else {
      print("el código del error" +
          apiResponse.statusResponse.toString() +
          " El mensaje de error es: " +
          apiResponse.message);
    }
    return apiResponse;
  }
}