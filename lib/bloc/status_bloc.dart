import 'package:flutter/cupertino.dart';
import 'package:proyectov2/modelo/api_response_model.dart';
import 'package:proyectov2/modelo/status_model.dart';
import 'package:proyectov2/repository/repository.dart';
import 'package:proyectov2/resource/constants.dart';

class StatusBloc{
  final Repository _repository = Repository();
  var _apiResponse = ApiResponse();


  ApiResponse get apiResponse =>_apiResponse;


  StatusBloc(BuildContext context);
   Future<ApiResponse> updateProduct(Status status) async {
    ApiResponse apiResponse = await _repository.actualizarStatus(status);
    if (apiResponse.statusResponse == 200) {
      apiResponse.message = Constants.createMessage;
      print(apiResponse.message);
    } else {
      print("el código del error" +
          apiResponse.statusResponse.toString() +
          " El mensaje de error es: " +
          apiResponse.message);
    }
    return apiResponse;
  }
}