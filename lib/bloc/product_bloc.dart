
import 'package:flutter/material.dart';
//import 'package:proyectov2/apiService/api_response_model.dart';
import 'package:proyectov2/modelo/api_response_model.dart';
import 'package:proyectov2/modelo/product_model.dart';
import 'package:proyectov2/repository/repository.dart';
import 'package:proyectov2/resource/constants.dart';

class ProductBloc {
  final Repository _repository = Repository();
    var _apiResponse = ApiResponse();

  ApiResponse get apiResponse => _apiResponse;

  ProductBloc(BuildContext context);
  Future<ApiResponse> createProduct(Product product) async {
    ApiResponse apiResponse = await _repository.registrarProducto(product);
    if (apiResponse.statusResponse == 200) {
      apiResponse.message = Constants.createMessage;
      print(apiResponse.message);
    } else {
      print("el código del error" +
          apiResponse.statusResponse.toString() +
          " El mensaje de error es: " +
          apiResponse.message);
    }
    return apiResponse;
  }
   /*Future<ApiResponse> updateProduct(Product product) async {
    ApiResponse apiResponse = await _repository.actualizarProduct(product);
    if (apiResponse.statusResponse == 200) {
      apiResponse.message = Constants.createMessage;
      print(apiResponse.message);
    } else {
      print("el código del error" +
          apiResponse.statusResponse.toString() +
          " El mensaje de error es: " +
          apiResponse.message);
    }
    return apiResponse;
  }*/
}
