import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:proyectov2/modelo/api_response_model.dart';
import 'package:proyectov2/modelo/discard_model.dart';
import 'package:proyectov2/resource/constants.dart';

class DiscardApiService{
Discard _discard;
DiscardApiService();
Future<ApiResponse> deleteDiscard(Discard discard)async{
  ApiResponse apiResponse = ApiResponse(statusResponse: 0);
  var body2 = json.encode(discard.toJson());
  Uri uri = Uri.http(Constants.urlAuthority, Constants.pathServiceStatusUpdate);
  var res= await http.put(uri,
  headers: {HttpHeaders.contentTypeHeader: Constants.contenTypeHeader},
  body: body2);

  var resBody = json.decode(res.body);
  apiResponse.statusResponse=res.statusCode;

  if(apiResponse.statusResponse==200){
    _discard = Discard.fromJson(resBody);
    apiResponse.object = _discard;
  }
  return apiResponse;
}

}