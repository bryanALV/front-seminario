import 'dart:convert';
import 'dart:io';
import 'package:proyectov2/modelo/api_response_model.dart';
import 'package:proyectov2/modelo/product_model.dart';
import 'package:proyectov2/resource/constants.dart';
import 'package:http/http.dart' as http;

class ProductApiService {
  Product _product;
  ProductApiService();

  Future<ApiResponse> insertProduct(Product product) async{
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body2 = json.encode(product.toJson());
    Uri uri = Uri.http(Constants.urlAuthority, Constants.pathServiceProduct);
    var res = await http.post(uri,
        headers: {HttpHeaders.contentTypeHeader: Constants.contenTypeHeader},
        body: body2);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      _product = Product.fromJson(resBody);
      apiResponse.object = _product;
    }
    return apiResponse;
  }
   Future<ApiResponse> updateProduct(Product product) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body2 = json.encode(product.toJson());
    Uri uri =
        Uri.http(Constants.urlAuthority, Constants.pathServiceDiscardDelete);
    var res = await http.put(uri,
        headers: {HttpHeaders.contentTypeHeader: Constants.contenTypeHeader},
        body: body2);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      _product = Product.fromJson(resBody);
      apiResponse.object = _product;
    }
    return apiResponse;
  }

}
    
