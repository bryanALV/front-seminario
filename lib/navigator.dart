import 'package:flutter/material.dart';
import 'package:proyectov2/resource/constants.dart';

class TecniNavigator{
 //static void goToHome(BuildContext context){
   //Navigator.of(context).pushNamedAndRemoveUntil(
     //Constants.homeRoute,(Route<dynamic> route)=>false);
// }

  static void goToRegistro(BuildContext context) {
    Navigator.pushNamed(context, Constants.registroRoute);
  }
  static void goToRegistroProduct(BuildContext context) {
    Navigator.pushNamed(context, Constants.productRoute);
  }

}