import 'package:flutter/material.dart';

import 'UI/Homepage.dart';

void main() => runApp(Myapp());

class Myapp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Registro de producto',
      debugShowCheckedModeBanner: false,
      home: Homepage(),
    );
  }
}