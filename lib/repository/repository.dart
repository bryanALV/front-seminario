import 'package:proyectov2/apiService/discard_api_service.dart';
import 'package:proyectov2/apiService/product_api_service.dart';
import 'package:proyectov2/apiService/status_api_service.dart';
import 'package:proyectov2/modelo/api_response_model.dart';
import 'package:proyectov2/modelo/discard_model.dart';
import 'package:proyectov2/modelo/product_model.dart';
import 'package:proyectov2/modelo/status_model.dart';

class Repository {
  
  final productApiService = ProductApiService();
  final statusApiService = StatusApiService();
  final discardApiService = DiscardApiService();

  Future<ApiResponse> registrarProducto(Product product)=> productApiService.insertProduct(product);
  Future<ApiResponse> actualizarStatus(Status status)=> statusApiService.updateStatus(status);
  Future<ApiResponse> eliminarproduct(Discard discard)=> discardApiService.deleteDiscard(discard);
}